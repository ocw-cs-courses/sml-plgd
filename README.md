## Functional Programming

http://www.cs.cmu.edu/~15150/index.html

Principles

> 1. Expressions must be well-typed.                     Well-typed expressions don't go wrong.
> 2. Every specification needs a proof.                   Proven programs do the right thing.
> 3. Every function needs a specification.              Specified programs are easier to understand.
> 4. Large programs should be modular.                Modular code is easier to maintain.
> 5. Data structures algorithms.                              Good representation can lead to better code.
> 6. Exploit parallelism.                                           Parallel code may run faster.
> 7. Strive for simplicity.                                          Programs should be as simple as possible, but no simpler.
