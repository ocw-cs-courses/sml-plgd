(* 15-150, Spring 2020               *)
(* Michael Erdmann & Frank Pfenning  *)
(* Code for Lecture 8: Sorting Trees *)

(************************************************************************)

(* compare : int * int -> order
   REQUIRES: true
   ENSURES:
     compare(x,y) ==> LESS    if x<y
     compare(x,y) ==> EQUAL   if x=y
     compare(x,y) ==> GREATER if x>y
*)
fun compare(x:int, y:int):order = 
   if x<y then LESS else
   if y<x then GREATER else EQUAL

datatype tree = Empty | Node of tree * int * tree


(* depth : tree -> int
   REQUIRES: true
   ENSURES: depth(t) ==> depth of tree t
*)
fun depth (Empty : tree) : int = 0
  | depth (Node(t1,x,t2)) = 1 + Int.max(depth t1, depth t2)

(* size : tree -> int
   REQUIRES: true
   ENSURES:  size(t) ==> number of nodes in t
*)
fun size (Empty : tree) : int = 0
  | size (Node(t1,x,t2)) = 1 + size t1 +  size t2


(* trav : tree -> int list
   REQUIRES: true
   ENSURES:  trav(t) evaluates to a list consisting of the integers in t,
             in the same order as seen during an in-order traversal of t.
*)
fun trav (Empty : tree) : int list = [ ]
  | trav (Node(t1, x, t2)) = trav t1 @ (x :: trav t2)


val t : tree = Node(Node(Node(Empty,7,Empty), 4, Node(Empty,1,Empty)),
                    3,
                    Node(Node(Empty,5,Empty), 6, Node(Empty,2,Empty)))

val [7,4,1,3,5,6,2] = trav t
val 3 = depth t
val 7 = size t



(* sorted : int list -> bool
   REQUIRES: true
   ENSURES:  sorted(L) evaluates to true if L is sorted and false otherwise.
*)
fun sorted ([ ] : int list) : bool = true
  | sorted [x] = true
  | sorted (x::y::L) = (compare(x,y) <> GREATER) andalso sorted(y::L)

(* Sorted : tree -> bool
   REQUIRES: true
   ENSURES:  Sorted(T) evaluates to true if T is sorted and false otherwise.
*)
fun Sorted (T : tree) : bool = sorted(trav T)



(* Ins : int * tree -> tree
   REQUIRES: t is a sorted tree
   ENSURES:  Ins(x,t) evaluates to a sorted tree such that
             trav(Ins(x,t)) is a sorted permutation of x::trav(t).
*)
fun Ins (x : int, Empty : tree) : tree = Node(Empty, x, Empty)
  | Ins (x, Node(t1, y, t2)) = 
     case compare(x,y) of
            GREATER   => Node(t1, y, Ins(x, t2))
          | _         => Node(Ins(x, t1), y, t2)



(* SplitAt : int * tree -> tree * tree
   REQUIRES: t is sorted
   ENSURES:  SplitAt(x,t) evaluates to a pair (t1,t2) of sorted trees
             such that:
                 (a) for every Node(_,y,_) in t1, compare(y,x)<>GREATER,
             and (b) for every Node(_,y,_) in t2, compare(y,x)<>LESS,
             and (c) trav(t1)@trav(t2) is a sorted permutation of trav(t).
*)
fun SplitAt(x : int, Empty : tree) : tree * tree = (Empty, Empty)
  | SplitAt(x, Node(left, y, right)) = 
     case compare(x, y) of
           LESS => let 
                      val (t1, t2) = SplitAt(x, left) 
                   in 
                      (t1, Node(t2, y, right)) 
                   end
          |  _  => let 
                      val (t1, t2) = SplitAt(x, right) 
                   in 
                      (Node(left, y, t1), t2) 
                   end


(* Merge : tree * tree -> tree
   REQUIRES: t1 and t2 are sorted
   ENSURES:  Merge(t1,t2) evaluates to a sorted tree t such that
             trav(t) is a sorted permutation of trav(t1)@trav(t2).
*)
fun Merge (Empty : tree, t2 : tree) : tree = t2
    (* We don't really need the next clause, but it speeds up a triviality. *)
  | Merge (t1, Empty) = t1   
  | Merge (Node(l1,x,r1), t2) = let 
                                   val (l2, r2) = SplitAt(x, t2) 
                                in 
                                   Node(Merge(l1, l2), x, Merge(r1, r2)) 
                                end


(* Msort : tree -> tree
   REQUIRES: true
   ENSURES:  Msort(t) evaluates to a sorted tree such that
             trav(Msort(t)) is a sorted permutation of trav(t).
*)
fun Msort (Empty : tree) : tree = Empty
  | Msort (Node(left, x, right)) = Ins (x, Merge(Msort left, Msort right))


val [1,2,3,4,5,6,7] = trav(Msort t)

val true = (Sorted o Msort) t



(* Alternative definition of Msort that does not use Ins: *)

(* Msort' : tree -> tree
   REQUIRES: true
   ENSURES:  Msort(t) evaluates to a sorted tree such that
             trav(Msort(t)) is a sorted permutation of trav(t).
*)
fun Msort' (Empty : tree) : tree = Empty
  | Msort' (Node(left, x, right)) = 
       Merge(Node(Empty, x, Empty), Merge(Msort' left, Msort' right))

val true = (Sorted o Msort') t

(* Msort and Msort' have the same asymptotic work and span, but the next
   test shows that they are not extensionally equivalent. *)
val false = (Msort t = Msort' t)   



(************************************************************************)
(* You are not required to know the rebalancing code that follows.      *)
(* It is here for those who might be interested.                        *)
(************************************************************************)

(* Without rebalancing, Msort can increase the tree depth significantly.
   For instance, consider the following balanced tree:

                      0
                     / \
                    /   \
                   4     1
                  / \   / \
                 6   5 3   2

   Msort will return the following tree, effectively a linear chain:

                       6
                      /
                     5
                    /
                   4
                  /
                 3
                /
               2
              /
             1
            /
           0
*)

fun s (x:int) : tree = Node(Empty, x, Empty)

val T = Node(Node(s 6, 4, s 5), 0, Node(s 3, 1, s 2))
val ST = Msort T

val 3 = depth T
val 7 = depth ST

val [0,1,2,3,4,5,6] = trav ST
val true = Sorted ST



(* Rebalancing code:                                                    *)
(* Caution:  As is, this code is inefficient.  In order to gain         *)
(* efficiency, one needs to redefine Node to contain a size field, thus *)
(* allowing for a constant time implementation of the size function.    *)

(* takeanddrop : tree * int -> tree * tree
   REQUIRES:  0 <= i <= size(T)
   ENSURES: takeanddrop(T,i) ==> (T1, T2) such that:
              (a) max(depth(T1), depth(T2)) <= depth(T)
              (b) size(T1) == i
              (c) trav(T) == trav(T1) @ trav(T2)
*)
fun takeanddrop (T : tree, 0 : int) : tree*tree = (Empty, T)
  | takeanddrop (Empty, _) = raise Fail "not enough elements"
  | takeanddrop (Node(left, x ,right), i) =
     (case Int.compare(i, size(left)) of
         LESS => let
                    val (t1,t2) : tree*tree = takeanddrop(left, i)
                 in
                    (t1, Node(t2, x, right))
                 end
       | EQUAL => (left, Node(Empty, x , right))
       | GREATER => let
                       val (t1,t2) : tree*tree = takeanddrop(right, i - 1 - size(left))
                    in
                       (Node(left, x, t1), t2)
                    end)

(* halves : tree -> tree * int * tree
   REQUIRES: T is not Empty
   ENSURES: halves(T) ==> (T1, T2) such that:
              (a) size(T1) == size(T) div 2
              (b) trav(t) == trav(T1) @ (x::trav(T2))
*)
fun halves (T : tree) : tree * int * tree =
     let
        val (T1, T2) : tree * tree = takeanddrop(T, (size T) div 2)
        val (Node(Empty, x, Empty), T3) : tree * tree = takeanddrop(T2, 1)
     in
        (T1, x, T3)
     end


(* rebalance : tree -> tree
   REQUIRES: true
   ENSURES: rebalance(T) ==> T' such that:
              (a) trav(T) == trav(T')
              (b) depth(T') == ceiling(log_2(size(T')))
*)
fun rebalance (Empty : tree) : tree = Empty
  | rebalance (T) =
      let
         val (left, x, right) : tree*int*tree = halves(T)
      in
         Node(rebalance left, x, rebalance right)
      end


(* Tests *)

val (Empty,Node(Empty,1,Empty)) =  takeanddrop (Node(Empty,1,Empty),0)

val (Node(Node(Empty,4,Empty),1,Empty),
     Node(Empty,0,Empty))=
     takeanddrop (Node(Node(Empty,4,Empty),1,Node(Empty,0,Empty)),2)

val (Node(Node(Empty,1,Empty),2,Node(Empty,3,Empty)),
    Node(Empty,4,Node(Node(Empty,5,Empty),6,Empty))) =
    takeanddrop (Node(Node(Node(Empty,1,Empty),2,Node(Empty,3,Empty)),
    4,Node(Node(Empty,5,Empty),6,Empty)),3)


(* makeleft : int -> tree
   REQUIRES:  n >= 0
   ENSURES: makeleft(n) produces a linear tree of length n,
            descending to the left.
*)
fun makeleft (0 : int) : tree = Empty
  | makeleft (n : int) : tree = Node(makeleft(n-1), n, Empty)

val left179 : tree = makeleft 179
val 179 = size left179
val 179 = depth left179

val rebleft179 = rebalance left179
val 179 = size rebleft179
val 8 = depth rebleft179


(* makeright : int -> tree
   REQUIRES:  n >= 0
   ENSURES: makeright(n) produces a linear tree of length n,
            descending to the right.
*)
fun makeright (0 : int) : tree = Empty
  | makeright (n : int) : tree = Node(Empty, n, makeright(n-1))

val right179 : tree = makeright 179
val 179 = size right179
val 179 = depth right179

val rebright179 = rebalance right179
val 179 = size rebright179
val 8 = depth rebright179

(************************************************************************)
