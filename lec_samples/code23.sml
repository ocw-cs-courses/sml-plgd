(* 15-150, Spring 2020                          *)
(* Michael Erdmann & Frank Pfenning             *)
(* Code for Lecture 23:  Imperative Programming *)

(************************************************************************)

(* Practice using cells: *)


val r = ref 0;
(* r : int ref *)
(* !r = 0 here *)

r := !r + 1;
(* !r = 1 here *)

val s = ref 1;
(* s : int ref, distinct from the value of r *)

s := !s + 1;
(* !r = 1, !s = 2 here *)

s := !s + 1;
(* !r = 1, !s = 3 here *)



(* We introduce an alias q for cell r: *)

val q = r;
(* q : int ref, q = r, !q = !r = 1 here *)



(* ref cells can be tested for identity: *)
val true = (q = r)
val false = (s = r);


q := !q + 1;
(* !q = !r = 2 *)

(************************************************************************)


(* Bank account example: *)

val account = ref 100

(* The following function uses pattern matching of the form ref (cur) 
   to bind the contents of a cell to a variable: 
*)

(* update : ('a -> 'a) -> 'a ref -> unit
   REQUIRES: true
   ENSURES:  update(f,r) changes the contents of r to f(!r).
*)
fun update (f: 'a -> 'a) (r : 'a ref) : unit =
    let
       val ref (cur) = r
    in
       r := f (cur)
    end

(* We could also have implemented updates as follows: *)

(*
fun update (f: 'a -> 'a) (r: 'a ref) : unit =
    r := f(!r)
*)



(* deposit : int -> int ref -> unit
   REQUIRES: true
   ENSURES:  deposit(n,a) increments the contents of a by n.
*)
fun deposit (n: int) (a: int ref) : unit =
     update (fn x => x + n) a


(* withdraw : int -> int ref -> unit
   REQUIRES: true
   ENSURES:  withdraw(n,a) decrements the contents of a by n.
*)
fun withdraw (n: int) (a: int ref) : unit =
     update (fn x => x - n) a;


(deposit 100 account; withdraw 50 account);
val 150 = !account;

(deposit 100 account; withdraw 50 account);
val 200 = !account

(* What if we evaluated (deposit 100 account) and (withdraw 50 account)
   in parallel?

e.g.:   Seq.tabulate (fn 0 => deposit 100 account
                       | 1 => withdraw 50 account) 2
*)


(************************************************************************)

(* Imperative list reversal: *)

fun fastrev (L : 'a list) : 'a list =
   let
     val R = ref [ ]
     fun rloop [ ] = !R
       | rloop (x::xs) = (R := x :: (!R); rloop xs)
   in
     rloop L
   end

val [1,2,3,4] = fastrev [4,3,2,1]


(************************************************************************)

(* Directed graph reachability *)

type  graph = int -> int list

val G : graph = fn 1 => [2,3]
                 | 2 => [1,3]
                 | 3 => [4]
                 | _ => [ ]

(* mem : int -> int list -> bool
   REQUIRES: true
   ENSURES   mem n L returns true if n is in L and false otherwise
*)
fun mem (n:int) = List.exists (fn x => n=x)

(* This will loop in cycles of g: *)
fun reachable1 (g:graph) (x:int, y:int) : bool =
     let
        fun dfs n = (n=y) orelse (List.exists dfs (g n))
     in
        dfs x
     end


val true = reachable1 G (3,4)
(* This will loop forever: *)
(*
val true = reachable1 G (1,4)
*)

(* Add a visited list in a reference cell that is in scope for all recursive calls: *)

(* reachable : graph -> int * int -> bool
   REQUIRES:  true
   ENSURES    reachable g (x,y) returns true if y is reachable from x in g,
              and returns false otherwise.

   dfs : int -> bool
   REQUIRES:  true
   ENSURES    dfs n returns true if y is reachable from n in g,
              and returns false otherwise.
*)

fun reachable (g:graph) (x:int, y:int) : bool =
     let
        val visited = ref []
        fun dfs n = (n=y) orelse
                          (not (mem n (!visited))
                                andalso
                          (visited := n::(!visited); List.exists dfs (g n)))
     in
        dfs x
     end


val true  =  reachable G (1,4)
val false =  reachable G (3,2)
val false =  reachable G (1,5)
val true  =  reachable G (3,4)


(* Just for fun, here is a purely functional implementation   *)
(* that passes the visited list as an argument to dfs and to  *)
(* a specialized (Boolean-valued) continuation.               *)

(* Spec for kreach is as for reachable, but the spec for dfs is now:

   dfs : int list -> int list -> (int list -> bool) -> bool

   REQUIRES:  k is total.
   ENSURES:   Consider (dfs frontier visited k).
                - Define graph g' to be identical to g except that
                  g'(v) is [] for each v in "visited".
                - Define R' to be the set of vertices reachable
                  from "frontier" in g'.

              Then (dfs frontier visited k) returns
                   true, if y is in R';
                   k(R' U visited), otherwise (here U means set union).
*)

fun kreach (g:graph) (x:int, y:int) : bool =
     let
        fun dfs [] visited k = k(visited)
          | dfs (n::ns) visited k =
             (n=y) orelse
             (if (mem n visited) 
              then (dfs ns visited k)
              else (dfs (g n) (n::visited) (fn vs => dfs ns vs k)))
     in
        dfs [x] [] (fn _ => false)
     end

val true  =  kreach G (1,4)
val false =  kreach G (3,2)
val false =  kreach G (1,5)
val true  =  kreach G (3,4)


(* One could also imagine a standard recursive implementation that *)
(* passes the visited list as an argument AND returns updated      *)
(* visited lists from those calls.                                 *)

(************************************************************************)

