(* 15-150, Spring 2020                                     *)
(* Michael Erdmann & Frank Pfenning                        *)
(* Code for Lecture 24:  Context-Free Grammars and Parsing *)

(************************************************************************)

(* A recursive-descent parser for a lambda-calculus-like language
   whose grammar is:

           E -->   lambda X . E
                 | (E E)
                 | X

           X --> <any alphanumeric string>

*)

(* Comment:
   Grammar above permits (X X).
   We could add nonterminals to separate the
   rules for E if if we don't like that.
*)



datatype token = LAMBDA | LPAREN | RPAREN | ID of string | DOT

datatype exp =   Fun of string * exp
               | App of exp * exp 
               | Var of string

exception ParseError



(* Here is a version of the parser based on continuations: *)

(* parseExp : token list -> (exp * token list -> 'a) -> 'a
   REQUIRES: true
   ENSURES:  (parseExp T k) ==> k(E,T2) if T == T1@T2 such that 
                    T1 is derivable in the grammar with abstract syntax E;
                raises ParseError otherwise.
*)

fun parseExp ((ID x)::ts) k = k(Var x, ts)

  | parseExp (LPAREN::ts) k = 
      parseExp ts (fn (e1, t1) => 
                     parseExp t1 (fn (e2, RPAREN::t2) => k(App(e1,e2), t2)
                                   | _ => raise ParseError))

  | parseExp (LAMBDA::(ID x)::DOT::ts) k =
      parseExp ts (fn (e, ts') => k(Fun(x,e), ts'))

  | parseExp _ _ = raise ParseError


(* parse : token list -> exp
   REQUIRES: true
   ENSURES:  parse(T) returns E if T is derivable in the grammar 
             with abstract syntax E; raises ParseError otherwise.
*)
   
fun parse tokens = parseExp tokens (fn (e,nil) => e | _ => raise ParseError)


(* Tests: *)

val App(Fun("x", Var("x")), Var("y")) = parse [LPAREN, LAMBDA, ID("x"), DOT, ID("x"), ID("y"), RPAREN]

val false = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x", RPAREN, ID "w", LAMBDA]; true) handle ParseError => false
val true = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x", RPAREN]; true) handle ParseError => false
val false = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x"]; true) handle ParseError => false
val false = (parse [LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x"]; true) handle ParseError => false
val false = (parse [ID"x", ID"x"]; true) handle ParseError => false
val Var"x" = parse [ID"x"]
val App(Var"x", Var"y") = parse [LPAREN, ID"x", ID"y", RPAREN]
val Fun("x", Fun("x", Var"x")) = parse [LAMBDA, ID"x", DOT, LAMBDA, ID"x", DOT, ID"x"]

val Fun("x", App(Var"x", Var"y")) = parse [LAMBDA, ID"x", DOT, LPAREN, ID"x", ID"y", RPAREN]



(************************************************************************)

(* Here is a version of the parser based on direct recursion: *)

(* parseExp : token list -> exp * token list
   REQUIRES: true
   ENSURES:  parseExp(T) ==> (E,T2) if T == T1@T2 such that 
                   T1 is derivable in the grammar with abstract syntax E;
               raises ParseError otherwise.
*)

fun parseExp ((ID x)::ts) = (Var x, ts)

  | parseExp (LPAREN::ts) = 
      let
         val (e1, t1) = parseExp ts
      in
         case parseExp t1 of
           (e2, RPAREN::t2) => (App(e1,e2), t2)
           | _ => raise ParseError
      end
 
  | parseExp (LAMBDA::(ID x)::DOT::ts) =
      let
         val (e, ts') = parseExp ts
      in
         (Fun(x,e), ts')
      end

  | parseExp _ = raise ParseError


(* parse : token list -> exp
   REQUIRES: true
   ENSURES:  parse(T) returns E if T is derivable in the grammar 
             with abstract syntax E; raises ParseError otherwise.
*)
   
fun parse tokens = 
      case parseExp tokens of
        (e,nil) => e
        | _ => raise ParseError


(* Same tests again: *)

val App(Fun("x", Var("x")), Var("y")) = parse [LPAREN, LAMBDA, ID("x"), DOT, ID("x"), ID("y"), RPAREN]

val false = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x", RPAREN, ID "w", LAMBDA]; true) handle ParseError => false
val true = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x", RPAREN]; true) handle ParseError => false
val false = (parse [LPAREN, LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x"]; true) handle ParseError => false
val false = (parse [LAMBDA, ID"x", DOT, ID"x", LAMBDA, ID"x", DOT, ID"x"]; true) handle ParseError => false
val false = (parse [ID"x", ID"x"]; true) handle ParseError => false
val Var"x" = parse [ID"x"]
val App(Var"x", Var"y") = parse [LPAREN, ID"x", ID"y", RPAREN]
val Fun("x", Fun("x", Var"x")) = parse [LAMBDA, ID"x", DOT, LAMBDA, ID"x", DOT, ID"x"]

val Fun("x", App(Var"x", Var"y")) = parse [LAMBDA, ID"x", DOT, LPAREN, ID"x", ID"y", RPAREN]


(************************************************************************)

