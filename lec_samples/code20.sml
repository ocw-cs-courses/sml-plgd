(* 15-150, Spring 2020                          *)
(* Michael Erdmann & Frank Pfenning             *)
(* Code for Lecture 20:  Two-Player Games, Nim. *)

(************************************************************************)

(* You need to have sequences loaded for this file to load properly.    *)
(* See writeup for detailed descriptions of the code given here.        *)

(* This code doesn't actually play any games yet, merely sets up Nim.   *)
(* There will be more in the next lecture and assignment.               *)


signature GAME =
sig
    datatype player = Minnie | Maxie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    type state  (* abstract, representing states of the game *)
    type move   (* abstract, representing moves of the game  *)

    val start : state

    (* REQUIRES:  m is in moves(s)                 *)
    (* ENSURES:   make_move(s,m) returns a value.  *)
    val make_move : state * move -> state 

    (* The next three functions are "views" of the abstract types state and move. *)

    (* REQUIRES: status(s) == In_play                                      *)
    (* ENSURES:  moves(s) returns a nonempty sequence of moves legal at s. *)
    val moves : state -> move Seq.seq 

    val status : state -> status
    val player : state -> player    (* says whose turn it is to make a move *)

    (* We have placed the estimator inside GAME for simplicity in these lectures.
       More generally, one would want to place it in a separate signature/structure.
    *)
    datatype est = Definitely of outcome | Guess of int

    (* REQUIRES:  status(s) == In_play          *)
    (* ENSURES:   estimate(s) returns a value.  *)
    val estimate : state -> est
    
    val stateToString : state -> string
    val statusToString : status -> string
    val estToString : est -> string
    val movesToString : move Seq.seq -> string

end  (* signature GAME *)


structure Nim : GAME = 
struct
    datatype player = Maxie | Minnie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    datatype state = State of int * player 
         (* The integer component is the number of pebbles left.  *)
         (* The player component is who should take pebbles next. *)
         
    datatype move = Move of int 
         (* The integer is how many pebbles to pick up.           *)

    val start = State (15, Maxie)
         (* Initial state for Nim has 15 pebbles, Maxie goes first. *)
        

    fun flip Maxie = Minnie 
      | flip Minnie = Maxie
         (* flip : player -> player *)
         (* Switches the player from Maxie to Minnie or vice versa. *)

    fun make_move (State (n, p), Move k) =
        if (n >= k) then State (n - k, flip p)
                       else raise Fail "tried to make an illegal move"
             

    fun moves (State (n , _)) = Seq.tabulate (fn k => Move(k+1)) (Int.min(n,3))
        
    fun player (State (_, p)) = p

    fun status (State (0, p)) = Over(Winner p)
      | status _ = In_play
        (* Nim is over when no pebbles are left.                        *)
        (* When game ends, whoever would have moved next is the winner. *)
        
        
    datatype est = Definitely of outcome | Guess of int
    
    fun estimate (State (n, p)) =
        if n mod 4 = 1 then Definitely (Winner (flip p))
                       else Definitely (Winner p)
       (* If there are n pebbles left, with n=1 (mod 4), then the player
          whose turn it is must lose, assuming optimal play by opponent.
          Otherwise, that player can win.                                *)


    fun stateToString (State(n, Maxie)) = "State(" ^ (Int.toString n) ^ ", Maxie)"
      | stateToString (State(n, Minnie)) = "State(" ^ (Int.toString n) ^ ", Minnie)"

    fun statusToString (Over(Draw)) = "Over(Draw)"
      | statusToString (Over(Winner(Maxie))) = "Over(Winner Maxie)"
      | statusToString (Over(Winner(Minnie))) = "Over(Winner Minnie)"
      | statusToString In_play = "In_play"

    fun estToString (Definitely(Draw)) = "(Definitely(Draw))"
      | estToString (Definitely(Winner(Maxie))) = "Definitely(Winner Maxie)"
      | estToString (Definitely(Winner(Minnie))) = "Definitely(Winner Minnie)"
      | estToString (Guess m) = "Guess(" ^ (Int.toString m) ^ ")"

    fun movesToString mseq = Seq.toString (fn (Move k) => "Move " ^ (Int.toString k)) mseq

end  (* structure Nim *)


structure DumbNim : GAME =
struct
    datatype player = Maxie | Minnie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    datatype state = State of int * player 
         (* The integer component is the number of pebbles left.  *)
         (* The player component is who should take pebbles next. *)
         
    datatype move = Move of int 
         (* The integer is how many pebbles to pick up.           *)

    val start = State (15, Maxie)
         (* Initial state for Nim has 15 pebbles, Maxie goes first. *)
        

    fun flip Maxie = Minnie 
      | flip Minnie = Maxie
         (* flip : player -> player *)
         (* Switches the player from Maxie to Minnie or vice versa. *)

    fun make_move (State (n, p), Move k) =
        if (n >= k) then State (n - k, flip p)
                       else raise Fail "tried to make an illegal move"
             

    fun moves (State (n , _)) = Seq.tabulate (fn k => Move(k+1)) (Int.min(n,3))
        
    fun status (State (0, p)) = Over(Winner p)
      | status _ = In_play
        (* Nim is over when no pebbles are left.                        *)
        (* When game ends, whoever would have moved next is the winner. *)
        
    fun player (State (_, p)) = p

        
    datatype est = Definitely of outcome | Guess of int
    
    (* This estimator is intentionally dumb. *)
    fun estimate (State (1, p)) = Definitely (Winner (flip p))
      | estimate _ = Guess 0
       (* If there is exactly 1 pebble left, then the player
          whose turn it is must lose.  Otherwise, guess a draw. *)
           

    fun stateToString (State(n, Maxie)) = "State(" ^ (Int.toString n) ^ ", Maxie)"
      | stateToString (State(n, Minnie)) = "State(" ^ (Int.toString n) ^ ", Minnie)"

    fun statusToString (Over(Draw)) = "Over(Draw)"
      | statusToString (Over(Winner(Maxie))) = "Over(Winner Maxie)"
      | statusToString (Over(Winner(Minnie))) = "Over(Winner Minnie)"
      | statusToString In_play = "In_play"

    fun estToString (Definitely(Draw)) = "(Definitely(Draw))"
      | estToString (Definitely(Winner(Maxie))) = "Definitely(Winner Maxie)"
      | estToString (Definitely(Winner(Minnie))) = "Definitely(Winner Minnie)"
      | estToString (Guess m) = "Guess(" ^ (Int.toString m) ^ ")"

    fun movesToString mseq = Seq.toString (fn (Move k) => "Move " ^ (Int.toString k)) mseq

end  (* structure DumbNim *)


(************************************************************************)



