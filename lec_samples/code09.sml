(* 15-150, Spring 2020                                 *)
(* Michael Erdmann & Frank Pfenning                    *)
(* Code for Lecture 9: Polymorphism and Type Inference *)

(************************************************************************)

(* zip : 'a list * 'b list -> ('a * 'b) list
   REQUIRES: true
   ENSURES:  zip([a1,a2,...,an],[b1,b2,...,bm]) ==>
                   [(a1,b1), (a2,b2), ..., (ak,bk)] with k = min(n,m) >= 0.
*)

fun zip ([] : 'a list, B : 'b list) : ('a * 'b) list = []
  | zip (A, []) = []
  | zip (a::A, b::B) = (a,b)::zip(A,B)

val nil : (bool * int) list = zip([],[1])
val nil : (bool * int) list = zip([true],[])
val [(true,3), (false, 15), (true, 17)] : (bool * int) list 
    = zip([true,false,true], [3,15,17])



(* lookup : ('a * 'a -> bool) * 'a * ('a * 'b) list -> 'b option
   REQUIRES: true
   ENSURES:  lookup(eq, x, L) evaluates to
               SOME(b) of the leftmost (a,b) in L for
                  which eq(x,a) is true, if there is such an (a,b);
               NONE otherwise.
*)

fun lookup(_ : 'a * 'a -> bool, _ : 'a, [] : ('a * 'b) list) : 'b option = NONE
  | lookup(eq, x, (a,b)::L) = if eq(x,a) then SOME(b) else lookup(eq, x, L)

val NONE = lookup(op =, 5, [(1,"a"), (2, "b"), (3, "c"), (4, "c")])
val SOME(3) = lookup(op =, "c", [("a", 1), ("b", 2), ("c", 3), ("c", 4)])




datatype 'a tree = Empty | Node of 'a tree * 'a * 'a tree

(* trav : 'a tree -> 'a list
   REQUIRES: true
   ENSURES:  trav(t) evaluates to a list consisting of the elements in t,
             in the same order as seen during an in-order traversal of t.
*)
fun trav (Empty : 'a tree) : 'a list = [ ]
  | trav (Node(t1, x, t2)) = trav t1 @ (x :: trav t2)


val t : string tree = 
              Node(Node(Node(Empty,"7",Empty), "4", Node(Empty,"1",Empty)),
                   "3",
                    Node(Node(Empty,"5",Empty), "6", Node(Empty,"2",Empty)))

val ["7","4","1","3","5","6","2"] = trav t


(************************************************************************)

(* Contrast the following two recursive function declarations.
   Both are well-typed.
   The first has type int -> int 
       since its body has type int assuming x:int and loop:int->int.
   The second has type real -> real 
       since its body has type real assuming x:real and loop:real->real.
*)

fun loop (x:int):int = loop(x)

fun loop (x:real):real = loop(x)

(************************************************************************)

(* Can you infer the most general types of the functions below? *)
(* Think about them, then doublecheck yourself using the REPL.  *)

fun increment x = x + 1

fun fst(x,y) = x

fun scd(x, y:int) = y

fun outer(x,y,z) = 2*(x+z)

fun twice f = f (f 0)

fun g x = g (g x)

fun h x = h x

fun id x = x

val (3, true, "hi") = (id 3, id true, id "hi")
val 43 = id (fn x => x + 1) 42
val 42 : int = id id 42


(* Here are some additional problems beyond what we did in lecture: *)

fun square (x:int) : int = x * x

fun sqrf (f : int -> int, x) = square (f x)

fun sqrg (g, x) = square (g x)

val 81 = sqrf(fn n => n + 2, 7)
val 81 = sqrg(fn n => n + 2, 7)

(************************************************************************)

(* What are the type and value (if there is a value) of the following
   expressions?   Again, think about the problem then try in the REPL.

   Also, be aware that function application associates to the left.
   So    f g x    is syntactic sugar for
        (f g) x   which is also the same as
        (f g)(x)  .
*)


(*
val whatvalue1 = id twice square
val whatvalue2 = twice id square
*)

(************************************************************************)

(* Some questions after class prompted me to include the following 
   "interesting" type inference problem: *)

(* select : 'a * 'a list -> 'a *)
fun select (_ :'a , x::_ : 'a list) : 'a = x
  | select (default, []) = default

(* Then: *)

val (7, "empty") : int*string = (select (0, [7,8,1]), select ("empty", []))

(* However, the following declaration would *not* be well-typed:  *)
(* Think about why not. *)

(*
fun pairselect (selector : 'a * 'a list -> 'a) =
     (selector (0, [7,8,1]), selector ("empty", []))
*)

(************************************************************************)
