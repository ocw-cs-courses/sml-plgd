(* 15-150, Spring 2020              *)
(* Michael Erdmann & Frank Pfenning *)
(* Code for Lecture 2: Functions *)

(************************************************************************)

(* Example of the 5-step methodology for writing a function: *)
(* square : int -> int
   REQUIRES:  true
   ENSURES:   square(x) evaluates to x * x
*)
fun square (x:int) : int = x * x

(* Testcases: *)
val 0 = square 0
val 49 = square 7
val 49 = square (~7)


(************************************************************************);

(* Here is an anonymous lambda expression -- it is a function value: *)

(fn (x:int) => x * x);

(* We could have used it inline to square an integer: *)

(fn (x:int) => x * x)(7);

(* That should evaluate to 49. *)


(************************************************************************)

(* pi up to 5 digits *)
val pi : real = 3.14159


(* area : real -> real
   REQUIRES:  true
   ENSURES:   real(r) computes the area of a disk with radius r, namely pi*r*r
*)
fun area (r:real):real = pi * r * r
  
val a2 : real = area (2.0)

(* We can shadow the definition of pi. *)
val pi : real = 0.0

(* The area function will remain unchanged. *)
val a3 : real = area (2.0)


(***********************************************************************)

(* All ML functions take a single argument.
   What if we want multiple arguments?  Or no arguments?
   Answer: Use tuples and unit, respectively:
*)


(* A function of two arguments is actually a function
   taking a pair as argument.                          *)

(* add : int * int -> int
   REQUIRES:  true
   ENSURES:   add(x,y) ==> x + y
*)

fun add(x:int, y:int) : int = x + y

(* We can also write functions that take "no arguments".
   Well, not quite, but we can write functions that take ():unit
   as an argument:
*)

fun alwaysthree (():unit) : int = 3

val 3 = alwaysthree ()

(***********************************************************************)

(* Returning a pair *)
(* Type definitions are transparent to type-checking *)

type float = real
type point = float * float

(* reflect : point -> point
   REQUIRES:  true
   ENSURES:   reflect(x,y) ==> (x, ~y)
*)

fun reflect ((x,y):point) : point = (x, ~y)

(* We can use pattern matching to extract the results: *)

val (a,b) : point = reflect (10.1, 15.7)

(* Now the environment contains bindings [10.1/a , ~15.7/b]. *)


(***********************************************************************)

(* Recursive Functions *)
(* Factorial: *)

(* fact : int -> int
   REQUIRES:  n >= 0
   ENSURES: fact(n) ==> n!
*)

(* Here we write fact using function clauses, based on pattern-matching: *)
fun fact(0:int):int = 1
  | fact(n:int):int = n * fact(n-1)

(* Let's try out some testcases: *)
val 1 = fact 0
val 720 = fact 6



(* The following is a slightly different implementation, in which
   we have changed the function clauses into a case expression: *)
fun fact(n:int):int =
  (case n of
      0 => 1
    | _ => n*fact(n-1))

(* Again, some testcases: *)
val 1 = fact 0
val 720 = fact 6


(***********************************************************************)

(* Fibonacci Numbers:   1,1,2,3,5,8,13,21, ... *)

(* fib: int -> int
   REQUIRES: n >= 0.
   ENSURES:  fib(n) ==> f_n, the nth Fibonacci number.
   Effects: none

   This definition requires O(fib(n)) calls to compute fib(n).
*)

fun fib(0:int):int = 1
  | fib(1:int):int = 1
  | fib(n:int):int = fib(n-1) + fib(n-2)


(* Testcases: *)
val  1 = fib 0
val  1 = fib 1
val 21 = fib 7


(* fibb: int -> int*int
   REQUIRES: n >= 0.
   ENSURES:  fibb(n) ==> (f_n, f_{n-1}),
                           the nth and {n-1}st Fibonacci numbers,
                               where we define f_{-1} = 0.
   This definition requires only O(n) calls to compute fibb(n).
*)

fun fibb (0:int):int*int = (1, 0)
  | fibb (n:int):int*int = 
      let
         val (a, b) : int*int = fibb (n-1)
      in
         (a + b, a)
      end

(* Testcases: *)
val (1, 1)   = fibb 1
val (21, 13) = fibb 7


(***********************************************************************)

(* Here is some more practice with case expressions. *)

(* The function computes something silly, but the code
   illustrates a useful property of case expressions:

      The ability to case on a tuple, 
      thereby avoiding ugly nested conditionals.
*)

(* silly : int -> int
   REQUIRES: true
   ENSURES:  silly(x) evaluates to
                              0 if x = 1, 
                              x if x < 1,
                        and x*x if x > 1.
*)

fun silly (x:int):int =
      (case (square x, x > 0) of
         (1, true)  => 0
       | (_, false) => x
       | (sqr, _)     => sqr)

val 0 = silly 1
val 0 = silly 0
val ~5 = silly ~5
val 49 = silly 7

(************************************************************************)

(* And, just for fun, here is a function that takes another function
   as an argument: *)

(* sqrf : (int -> int) * int -> int
   REQUIRES: true
   ENSURES:  sqrf (f, x) ==> (f(x))*(f(x))
*)

fun sqrf (f : int -> int, x : int) : int = square(f(x))

val 81 = sqrf (fn (n:int) => n + 2, 7)

(************************************************************************)

