(* 15-150, Spring 2020                               *)
(* Michael Erdmann & Frank Pfenning                  *)
(* Code for Lecture 16:                              *)
(* Modules: Signatures and Structures                *)
(* Abstraction Functions, Representation Invariants. *)

(************************************************************************)

(* Queue data structure *)
signature QUEUE =
sig
  type 'a queue        (* abstract type *)

  val empty : 'a queue
  val enq : 'a queue * 'a -> 'a queue
  val null : 'a queue -> bool

  exception Empty

  (* deq (q) raises Empty if q is empty *)
  val deq : 'a queue -> 'a * 'a queue
end

(* Queue1 gives an inefficient but straightforward implementation. *)

(* Observe that Queue1: QUEUE, i.e, we use transparent ascription.
   This is useful for printing values, but one may want to hide
   the internal implementation by using opaque ascription:
     structure Queue1 :> QUEUE
   Try it.

  Abstraction function:
    A queue is implemented as a list consisting
    of the queue elements in arrival order.

  Representation Invariants:  none
*)
structure Queue1 : QUEUE =
struct
  type 'a queue = 'a list

  val empty = nil

  fun enq (q,x) = q @ [x]

  val null = List.null

  exception Empty

  (* deq (q) raises Empty if q is empty *)
  fun deq (x::q) = (x,q)
    | deq (nil) = raise Empty
end


(* Let's test inside a structure: *)
structure Queue1Tests =
struct
  val queue0 : int Queue1.queue = Queue1.empty
  val queue1 : int Queue1.queue = Queue1.enq(queue0, 1)
  val queue2 : int Queue1.queue = Queue1.enq(queue1, 2)
  val queue2': int Queue1.queue = Queue1.enq(queue1, 2000)

  val (a, queue3)  = Queue1.deq(queue2)
  val (b, queue3') = Queue1.deq(queue2')

  val 1 = a
  val 1 = b
end


(* Queue2 gives a more efficient implementation. *)
(* Here we again use transparent ascription:
     structure Queue2 : QUEUE

   Also try it with opaque ascription:
     structure Queue2 :> QUEUE

  Abstraction function:
    A queue is implemented as a pair of lists (front, back)
    such that

          front @ (rev back)

    consists of the queue elements in arrival order.

  Representation Invariants:  none
*)

structure Queue2 : QUEUE =
struct
  type 'a queue = 'a list * 'a list

  val empty = (nil, nil)

  fun enq ((front, back), x) = (front, x::back)

  fun null (nil,nil) = true
    | null _ = false

  exception Empty

  (* deq (q) raises Empty if q is empty *)
  fun deq (x::front, back) = (x, (front, back))
    | deq (nil, nil) = raise Empty
    | deq (nil, back) = deq(rev back, nil)

end

structure QueueTests =
struct
  (* An abbreviation for the structure name *)
  (* Try also: structure Q = Queue1 *)
  structure Q = Queue2

  val q0 : int Q.queue = Q.empty
  val q1 = Q.enq (q0, 1)
  val q2 = Q.enq (q1, 2)
  val q3 = Q.enq (q2, 3)
  val (x1, q4) = Q.deq (q3)
  val q4' = Q.enq (q4, 4)
  val q5  = Q.enq (q4', 5)
  val (x2, q6) = Q.deq (q5)
  (* Previous queues are still accessible *)
  val (x1', q4'') = Q.deq (q3)

  val 1 = x1
  val 2 = x2
  val 1 = x1'


  (* We can shadow previous queues for garbage collection *)
  (* Only the last one is accessible, others are shadowed *)
  val q = Q.empty
  val q = Q.enq (q, "a")
  val q = Q.enq (q, "b")
  val q = Q.enq (q, "c")
  val (s1, q) = Q.deq (q)
  val (s2, q) = Q.deq (q)

  val "a" = s1
  val "b" = s2


  (* Queues of optional integers *)
  datatype 'a option = NONE | SOME of 'a
  type intOptQueue = int option Q.queue

  (* Queues of integers or reals *)
  datatype number = Int of int | Real of real
  type numberQueue = number Q.queue
end


(************************************************************************)

(* Signature for dictionaries *)
(*
   For simplicity, we assume keys are strings, while stored entries
   are of arbitrary type.  This is prescribed in the signature.

   Existing entries may be "updated" by inserting a new entry
   with the same key.   (Thus there are no duplicate keys.)
*)

signature DICT =
sig
  type key = string                 (* concrete type *)
  type 'a entry = key * 'a          (* concrete type *)

  type 'a dict                      (* abstract type *)

  val empty : 'a dict
  val lookup : 'a dict -> key -> 'a option
  val insert : 'a dict * 'a entry -> 'a dict
end


(************************************************************************)

(* Association list implementation of dictionaries (popular in Lisp).
   Here we use opaque ascription to hide implementation details of
   the abstract type. *)

structure AssocList :> DICT =
struct
  type key = string
  type 'a entry = key * 'a

  type 'a dict = ('a entry) list

  (* 
    Abstraction function:
      A dictionary is implemented as a list of entries.

    Representation Invariant:
      If there are multiple entries with the same key,
      only the leftmost entry is valid.
  *)

  val empty = nil

  fun lookup l key =
    (* lk l ==> SOME(leftmost entry in l matching key) or NONE *)
    let fun lk nil = NONE
	  | lk ((key1,value1)::l) = 
              if (key = key1) then SOME(value1) 
              else lk l
    in 
      lk l
    end

  (* Inserting shadows previous entries with same key *)
  fun insert (l, entry) = entry::l
end  (* structure AssocList *)


(************************************************************************)

(* Binary search tree implementation of dictionaries.
   We use transparent ascription.  But then we use an internal datatype
   to prevent a user from manipulating the implementation directly.
*)

structure BinarySearchTree : DICT =
struct
  type key = string
  type 'a entry = string * 'a

  datatype 'a tree = Empty | Node of 'a tree * 'a entry * 'a tree

  type 'a dict = 'a tree

  (* 
    Abstraction function:
      The collection of entries in the binary tree constitutes
      the dictionary.

    Representation Invariant:
     The tree is sorted:
       For every node  Node(left, (key1,value1), right),
       every key in left is LESS than key1, and
       every key in right is GREATER than key1.
  *)

  val empty = Empty

  fun lookup tree key =
    (* lk : 'a dict -> 'a option                         *)
    (* lk (tree) searches for an entry with key in tree. *)
    let fun lk (Empty) = NONE
	  | lk (Node(left, (key1,value1), right)) =
              (case String.compare(key,key1)
		 of EQUAL => SOME(value1)
	          | LESS => lk left
	          | GREATER => lk right)
    in
      lk tree
    end

  fun insert (tree, entry as (key,_)) =
    (* ins : 'a dict -> 'a dict           *)
    (* ins (tree) inserts entry into tree *)
    let fun ins (Empty) = Node(Empty, entry, Empty)
	  | ins (Node(left, entry1 as (key1,_), right)) =
              (case String.compare(key,key1)
		 of EQUAL => Node(left, entry, right)
	          | LESS => Node(ins left, entry1, right)
                  | GREATER => Node(left, entry1, ins right))
    in 
      ins tree
    end
end  (* structure BinarySearchTree *)

(* Some Examples and Tests: *)
structure BSTTests =
struct
  structure BST = BinarySearchTree
  val d0 = BST.empty
  val d1 = BST.insert (d0, ("a",1))
  val d2 = BST.insert (d1, ("b",2))
  val d3 = BST.insert (d2, ("c",3))
  val d4 = BST.insert (d3, ("d",4))

  val NONE = BST.lookup d4 "e"
  val SOME(1) = BST.lookup d4 "a"

  (* or, using the curried form of lookup: *)

  val look4 = BST.lookup d4
  val NONE = look4 "e"
  val SOME(1) = look4 "a"
end

(************************************************************************)


(* Some code excerpted from "Modules.{tex,pdf}" by Steve Brookes: *)  

signature ARITH = 
 sig
     type integer
     val rep : int -> integer
     val display : integer -> string
     val add : integer * integer -> integer
     val mult : integer * integer -> integer
 end

structure Ints : ARITH =
struct
    type integer = int
    fun rep (n:int):integer = n
    fun display (n:integer):string = Int.toString n
    val add:integer * integer -> integer = (op +)
    val mult:integer * integer -> integer = (op * )
 end

structure Dec : ARITH =
 struct
   type digit = int   (* use only digits 0,1,2,3,4,5,6,7,8,9 *)
   type integer = digit list
     
   fun rep 0 = [ ]  |  rep n = (n mod 10) :: rep (n div 10)

   (* carry : digit * integer -> integer *)
   fun carry (0, ps) = ps
    |  carry (c, [ ]) = [c]
    |  carry (c, p::ps) = ((p+c) mod 10) :: carry ((p+c) div 10, ps)

   fun add ([ ], qs) = qs
    |  add (ps, [ ]) = ps
    |  add (p::ps, q::qs) = 
          ((p+q) mod 10) :: carry ((p+q) div 10, add(ps,qs))

   (* times : digit -> integer -> integer *)
   fun times 0 qs = [ ]
    |  times k [ ] = [ ]
    |  times k (q::qs) = 
         ((k * q) mod 10) :: carry ((k * q) div 10, times k qs)

   fun mult  ([ ], _) = [ ]
    |  mult (_, [ ]) = [ ]
    |  mult (p::ps, qs) = add (times p qs, 0 :: mult (ps,qs))
   
   fun display [ ] = "0" 
     | display L = foldl (fn (d, s) => Int.toString d ^ s) "" L
 end

(* inv : int list -> bool *)
fun inv [ ] = true
  | inv (d::L) = 0 <= d andalso d <= 9 andalso inv L

(* eval : int list -> int *)
fun eval [ ] = 0
  | eval (d::L) = d + 10 * eval(L)


(* fact : int -> Dec.integer *)
fun fact 0 = Dec.rep 1 
  | fact n = Dec.mult(Dec.rep n, fact(n-1))

(* To see all digits you may need to increase your printable string depth:
Control.Print.stringDepth := 2000
*)


val "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000"
   = Dec.display(fact 100)



structure Bin : ARITH =
 struct
   type digit = int   (* use only 0 and 1 *)
   type integer = digit list
   fun rep 0 = [ ]  |   rep n = (n mod 2) :: rep (n div 2)

   (* carry : digit * integer -> integer *)
   fun carry (0, ps) = ps
    |  carry (c, [ ]) = [c]
    |  carry (c, p::ps) = ((p+c) mod 2) :: carry ((p+c) div 2, ps)

   fun add ([ ], qs) = qs
    |  add (ps, [ ]) = ps
    |  add (p::ps, q::qs) = 
          ((p+q) mod 2) :: carry ((p+q) div 2, add (ps,qs))

   (* times : digit -> integer -> integer *)
   fun times 0 qs = [ ]
    |  times k [ ] = [ ]
    |  times k (q::qs) = 
            ((k * q) mod 2) :: carry ((k * q) div 2, times k qs)

   fun mult  ([ ], _) = [ ]
    |  mult (_, [ ]) = [ ]
    |  mult (p::ps, qs) = add (times p qs, 0 :: mult (ps,qs))

   fun display [ ] = "0" 
     | display L =  foldl (fn (d, s) => Int.toString d ^ s) "" L
 end


(* bfact : int -> Bin.integer *)
fun bfact 0 = Bin.rep 1 
  | bfact n = Bin.mult(Bin.rep n, bfact(n-1))


val "110110011000010010110010011101100001110010101110111000010010000000110100101010010100011010101010010111011110110100100000011010001011011101001011001101110111110011010011100001110101100100001101101011011001010010100001110100011001000011100110111110001000000111001000101110100010101010111000011001100101010010100001000001100011011101100101100111011011100101110110100101110111010001011000000101110101000100111001101011100011000011010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    = Bin.display(bfact 100)

(************************************************************************)

