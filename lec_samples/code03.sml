(* 15-150, Spring 2020                         *)
(* Michael Erdmann & Frank Pfenning            *)
(* Code for Lecture 3: Recursion and Induction *)

(************************************************************************)

(* power : (int * int) -> int 
   REQUIRES: k >= 0
   ENSURES: power(n,k) ==> n^k, with 0^0 = 1.
*)

fun power (_:int, 0:int) : int = 1
  | power (n:int, k:int) : int = n * power(n, k-1)

(* Testcases: *)

val 1 = power(7,0)
val 8 = power(2,3)
val 9 = power(3,2)
val 128 = power(2,7)

(* 
   In class, we used Mathematical Induction to prove that power(n,k)
   computes n^k, as specified, when called with an integer value n
   and a nonnegative integer value k.
*)

(* 
   power is not a very efficient implementation.  
   It requires O(k) multiplies.
   By using repeated squaring we can obtain an implementation
   that requires only O(log(k)) multiplies.
*)

(* even : int -> bool
   REQUIRES: true
   ENSURES: even(k) evaluates to true if k is even
                    evaluates to false if k is odd.
*)

fun even (k:int) : bool = ((k mod 2) = 0)


(* square : int -> int
   REQUIRES: true
   ENSURES: square(n) ==> n^2
*)
fun square (n:int) : int = n * n


(* Let's call this next function powere,
   just to have a different name,
   with the suffix "e" for "efficient".
*)

(* powere : (int * int) -> int 
   REQUIRES: k >= 0
   ENSURES: powere(n,k) ==> n^k, with 0^0 = 1.

   powere computes n^k using O(log(k)) multiplies.
*)

fun powere (_:int, 0:int) : int = 1
  | powere (n:int, k:int) : int =
      if even(k)
      then square(powere(n, k div 2))
      else n * powere(n, k-1)

val 1 = powere(7,0)
val 8 = powere(2,3)
val 9 = powere(3,2)
val 128 = powere(2,7)

(* 
   In class, we used Strong Induction to prove that powere(n,k)
   computes n^k, as specified, when called with an integer value n
   and a nonnegative integer value k.
*)

(* A comment about style:

   Some people dislike case expressions that have true | false patterns,
   arguing that if-then-else expressions are more elegant in such situations.
   That is why we wrote powere with an if-then-else expression.
   
   Other people argue that if-then-else expressions are hold-overs from
   imperative thinking, and that in a functional language one should always
   case over the values of a space.  For Booleans, the space simply has
   two possible values.

   If we did want to rewrite powere with a case expression, here is how:
*)

fun powere (_:int, 0:int) : int = 1
  | powere (n:int, k:int) : int =
      case even(k) of
         true  => square(powere(n, k div 2))
       | false => n * powere(n, k-1)

val 1 = powere(7,0)
val 8 = powere(2,3)
val 9 = powere(3,2)
val 128 = powere(2,7)


(************************************************************************)

(* 
   We started our discussion of structural induction for lists.
*)

(* int lists *)

val nil : int list = []

val fouritems : int list = [1,2,3,4]

(* length : int list -> int
   REQUIRES: true
   ENSURES: length(L) returns the number of integers in L.
*)

fun length ([]  : int list) : int = 0
  | length (x::xs) = 1 + length(xs)

val 0 = length []
val 3 = length [1,4,8]
val 4 = length fouritems

(* In lecture, we used structural induction on lists to prove that
   length is total, i.e., that
   length(L) always reduces to a value when L is a value of type int list.
*)

(************************************************************************)

