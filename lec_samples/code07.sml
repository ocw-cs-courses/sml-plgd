(* 15-150, Spring 2020               *)
(* Michael Erdmann & Frank Pfenning  *)
(* Code for Lecture 7: Sorting Lists *)

(************************************************************************)

(* compare : int * int -> order
   REQUIRES: true
   ENSURES:
     compare(x,y) ==> LESS    if x<y
     compare(x,y) ==> EQUAL   if x=y
     compare(x,y) ==> GREATER if x>y
*)
fun compare(x:int, y:int):order = 
   if x<y then LESS else
   if y<x then GREATER else EQUAL

(* This compare function is predefined in SML as Int.compare *)

(* We say that a list of integers is _sorted_ if and only if  *)
(* compare(x,y) evaluates to either LESS or EQUAL             *)
(* whenever x appears to the left of y in the list.           *)


(* sorted : int list -> bool
   REQUIRES: true
   ENSURES:  sorted(L) evaluates to true if L is sorted and to false otherwise.
*)
fun sorted ([ ] : int list) : bool = true
  | sorted [x] = true
  | sorted (x::y::L) = (compare(x,y) <> GREATER) andalso sorted(y::L)


(* Insertion sort *)

(* ins : int * int list -> int list
   REQUIRES: L is sorted
   ENSURES:  ins(x, L) evaluates to a sorted permutation of x::L
*)
fun ins (x : int, [ ] : int list) : int list = [x]
  | ins (x, y::L) = case compare(x, y) of
                        GREATER => y::ins(x, L)
                      |  _      => x::y::L


(* isort : int list -> int list
   REQUIRES: true
   ENSURES: isort(L) evaluates to a sorted permutation of L
*)
fun isort ([ ] : int list) : int list = [ ]
  | isort (x::L) = ins (x, isort L)


val [1,2,3,4] = isort[3,1,4,2]


(* Merge Sort *)

(* split : int list -> int list * int list
   REQUIRES: true
   ENSURES:  split(L) evaluates to a pair of lists (A, B) such that
             length(A) and length(B) differ by at most 1,
             and A@B is a permutation of L.
*)
fun split ([ ] : int list) : int list * int list = ([ ], [ ])
  | split [x] = ([x], [ ])
  | split (x::y::L) = 
       let 
          val (A, B) = split L 
       in 
          (x::A, y::B) 
       end


(* merge : int list * int list -> int list
   REQUIRES: A and B are sorted lists
   ENSURES:  merge(A,B) evaluates to a sorted permutation of A@B
*)
fun merge ([ ] : int list, B : int list) : int list = B
  | merge (A, [ ]) = A
  | merge (x::A, y::B) = case compare(x,y) of
                               LESS => x :: merge(A, y::B)
                             | EQUAL => x::y::merge(A, B)
                             | GREATER => y :: merge(x::A, B)


(* msort : int list -> int list
   REQUIRES: true
   ENSURES:  msort(L) evaluates to a sorted permutation of L
*)
fun msort ([ ] : int list) : int list = [ ]
  | msort [x] = [x]
  | msort L = 
      let 
         val (A, B) = split L
      in 
         merge(msort A, msort B)
      end


val [1,2,3,4] = msort[3,1,4,2]

(************************************************************************)

