(* 15-150, Spring 2020              *)
(* Michael Erdmann & Frank Pfenning *)
(* Code for Lecture 1               *)

(**********************************************************************)
(* Evaluation and Typing *)

(* Integers *)
~1 : int; (* NOT -1 *)
4 : int;
~3+2 : int;
5 div 2 : int;

(* Reals *)
3.14 : real;
5.0 / 2.0 : real; (* NOT 5 / 2 *)

(* Booleans *)
true : bool;
false : bool;

(* Typing and Evaluation *)
(* Some ill-typed expressions *)
(*
2.0 + 1;
true + 0;
*)

(* A well-typed expression without a value *)
(*
1 div 0;
*)

(* The following expression has type int and value 17: *)
if 3 > 4 then 10 div 0 else 17;

(* The following expression has type int but no value: *)
(*
if 3 < 4 then 10 div 0 else 17;
*)


(* Some tuples and their product types:  *)

(3.14, false, "ab", 4-1) : real * bool * string * int;

(1 + 6 + 7, "ab" ^ "cd") : int * string;

(#"a", 1, 3.14) : char * int * real;

(#"a", (1, 3.14)) : char * (int * real);


(**********************************************************************)
(* Binding and Scope *)

val x : int = 8 - 5
val y : int = x + 1
val x : int = 10
val z : int = x + 1


(* pi up to 5 digits *)
val pi : real = 3.14159;

(* Local bindings *)

let
   val m:int = 3
   val n:int = m*m
in
   m*n
end;


(* Do you understand the scope of "k" in the code below?         *)
(* What is the value bound to k by the first declaration below?  *)
(* What are the values of the subsequent two expressions?        *)

val k : int = 4;

let 
   val k : int = 3
in
   k*k
end;

k;

(**********************************************************************)

(* Example of the 5-step methodology for writing a function: *)
(* square : int -> int
   REQUIRES:  true
   ENSURES:   square(x) evaluates to x * x
*)
fun square (x:int) : int = x * x

(* Testcases: *)
val 0 = square 0
val 49 = square 7
val 49 = square (~7)

(**********************************************************************)

