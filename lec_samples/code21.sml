(* 15-150, Spring 2020                             *)
(* Michael Erdmann & Frank Pfenning                *)
(* Code for Lecture 21:  Implementing Game Playing *)

(************************************************************************)

(* You need to have sequences loaded for this file to load properly.    *)

signature GAME =
sig
    datatype player = Minnie | Maxie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    type state  (* abstract, representing states of the game *)
    type move   (* abstract, representing moves of the game  *)

    val start : state

    (* REQUIRES:  m is in moves(s)                 *)
    (* ENSURES:   make_move(s,m) returns a value.  *)
    val make_move : state * move -> state 

    (* The next three functions are "views" of the abstract types state and move. *)

    (* REQUIRES: status(s) == In_play                                      *)
    (* ENSURES:  moves(s) returns a nonempty sequence of moves legal at s. *)
    val moves : state -> move Seq.seq 

    val status : state -> status
    val player : state -> player    (* says whose turn it is to make a move *)

    (* For first-time simplicity, the estimator appears within GAME here.
       More generally, one would move it out of GAME and either put
       it into SETTINGS or PLAYER (signatures defined below) or into yet
       a fourth separate signature.
    *)
    datatype est = Definitely of outcome | Guess of int

    (* REQUIRES:  status(s) == In_play          *)
    (* ENSURES:   estimate(s) returns a value.  *)
    val estimate : state -> est
    

    (* The stringifying functions are slightly different   *)
    (* here than they are in the previous lecture's code:  *)

    val outcome_to_string : outcome -> string
    val player_to_string : player -> string
    val move_to_string : move -> string
    val state_to_string : state -> string
    val est_to_string : est -> string

  (* REQUIRES: string is single line and *not* terminated by newline.        *)
  (* ENSURES:  move described by string is legal at state, else return NONE. *)
    val parse_move : state -> string -> move option

end  (* signature GAME *)


structure Nim : GAME = 
struct
    datatype player = Maxie | Minnie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    datatype state = State of int * player 
         (* The integer component is the number of pieces left.  *)
         (* The player component is who should take pieces next. *)
         
    datatype move = Move of int 
         (* The integer is how many pieces to pick up.           *)

    val start = State (15, Maxie)
         (* Initial state for Nim has 15 pieces, Maxie goes first. *)
        

    fun flip Maxie = Minnie 
      | flip Minnie = Maxie
         (* flip : player -> player *)
         (* Switches the player from Maxie to Minnie or vice versa. *)

    fun make_move (State (n, p), Move k) =
        if (n >= k) then State (n - k, flip p)
                       else raise Fail "tried to make an illegal move"
             

    fun moves (State (n , _)) = Seq.tabulate (fn k => Move(k+1)) (Int.min(n,3))
        
    fun status (State (0, p)) = Over(Winner p)
      | status _ = In_play
        (* Nim is over when no pieces are left.                        *)
        (* When game ends, whoever would have moved next is the winner. *)
        
    fun player (State (_, p)) = p

        
    datatype est = Definitely of outcome | Guess of int
    
    fun estimate (State (n, p)) =
        if n mod 4 = 1 then Definitely (Winner (flip p))
                       else Definitely (Winner p)
       (* If there are n pieces left, with n=1 (mod 4), then the player
          whose turn it is must lose, assuming optimal play by opponent.
          Otherwise, that player can win.                                *)


    fun outcome_to_string (Winner Maxie) = "Maxie wins!"
      | outcome_to_string (Winner Minnie) = "Minnie wins!"
      | outcome_to_string (Draw) = "Game tied!"

    fun player_to_string Maxie = "Maxie"
      | player_to_string Minnie = "Minnie"

    fun state_to_string (State (n, p)) = 
        Int.toString n ^ " pieces left, and " ^ player_to_string p ^ "'s turn"

    fun move_to_string (Move k) = Int.toString k

    fun est_to_string (Definitely outcome) = outcome_to_string outcome
      | est_to_string (Guess n) = Int.toString n


    fun parse_move (State (n, _)) str = 
        let 
           fun enough k = if k <= n then SOME(Move k) else NONE
        in 
           case str of 
                "1" => enough 1
              | "2" => enough 2
              | "3" => enough 3
              | _   => NONE
        end
end  (* structure Nim *)


structure DumbNim : GAME =
struct
    datatype player = Maxie | Minnie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    datatype state = State of int * player 
         (* The integer component is the number of pieces left.  *)
         (* The player component is who should take pieces next. *)
         
    datatype move = Move of int 
         (* The integer is how many pieces to pick up.           *)

    val start = State (15, Maxie)
         (* Initial state for Nim has 15 pieces, Maxie goes first. *)
        

    fun flip Maxie = Minnie 
      | flip Minnie = Maxie
         (* flip : player -> player *)
         (* Switches the player from Maxie to Minnie or vice versa. *)

    fun make_move (State (n, p), Move k) =
        if (n >= k) then State (n - k, flip p)
                       else raise Fail "tried to make an illegal move"
             

    fun moves (State (n , _)) = Seq.tabulate (fn k => Move(k+1)) (Int.min(n,3))
        
    fun status (State (0, p)) = Over(Winner p)
      | status _ = In_play
        (* Nim is over when no pieces are left.                        *)
        (* When game ends, whoever would have moved next is the winner. *)
        
    fun player (State (_, p)) = p

        
    datatype est = Definitely of outcome | Guess of int
    
    (* This estimator is intentionally fairly dumb. *)
    fun estimate (State (1, p)) = Definitely (Winner (flip p))
      | estimate _ = Guess 0
       (* If there is exactly 1 piece left, then the player
          whose turn it is must lose.  Otherwise, guess a draw. *)


    fun outcome_to_string (Winner Maxie) = "Maxie wins!"
      | outcome_to_string (Winner Minnie) = "Minnie wins!"
      | outcome_to_string (Draw) = "Game tied!"

    fun player_to_string Maxie = "Maxie"
      | player_to_string Minnie = "Minnie"

    fun state_to_string (State (n, p)) = 
        Int.toString n ^ " pieces left, and " ^ player_to_string p ^ "'s turn"

    fun move_to_string (Move k) = Int.toString k

    fun est_to_string (Definitely outcome) = outcome_to_string outcome
      | est_to_string (Guess n) = Int.toString n


    fun parse_move (State (n, _)) str = 
        let 
           fun enough k = if k <= n then SOME(Move k) else NONE
        in 
           case str of 
                "1" => enough 1
              | "2" => enough 2
              | "3" => enough 3
              | _   => NONE
        end
end  (* structure DumbNim *)


signature PLAYER =
sig
    structure Game : GAME   (* parameter *)

    (* REQUIRES: Game.status(s) == In_play                         *)
    (* ENSURES:  next_move(s) evaluates to a Game move legal at s. *)
    val next_move : Game.state -> Game.move
end


signature SETTINGS =
sig
    structure Game : GAME  (* parameter *)
    val depth : int
end


(* Side note:
   We use transparent ascription to facilitate sharing constraints in TWO_PLAYERS.
   Alternative: opaque ascription with some 'where type' clauses here would work too.
*)
functor FullMiniMax (G : GAME) : PLAYER =
struct
    structure Game = G

    type edge = G.move * G.est
    fun edgemove (m,v) = m
    fun edgeval (m,v) = v

    (* lesseq : G.est * G.est -> bool *)
    fun lesseq(x, y) = (x=y) orelse 
         case (x, y) of
              (G.Definitely(G.Winner G.Minnie), _) => true
            | (_, G.Definitely(G.Winner G.Maxie)) => true
            | (G.Guess n, G.Definitely G.Draw) => (n <= 0)
            | (G.Definitely G.Draw, G.Guess m) => (0 <= m)
            | (G.Guess n, G.Guess m) => (n <= m)
            | (_, _) => false

    (* max : edge * edge -> edge   ,    min : edge * edge -> edge *)
    (* Note:  when values are equal, we stick with first edge.    *)
    fun max (e1, e2) = if lesseq(edgeval e2, edgeval e1) then e1 else e2
    fun min (e1, e2) = if lesseq(edgeval e1, edgeval e2) then e1 else e2

    (* choose : G.player -> edge Seq.seq -> edge *)
    fun choose G.Maxie  = Seq.reduce1 max
      | choose G.Minnie = Seq.reduce1 min

    (* search : G.state -> edge       *)
    (* REQUIRES: status(s) == In_play *)
    fun search (s : G.state) : edge =
        choose (G.player s)
               (Seq.map
                (fn m => (m, evaluate (G.make_move(s,m))))
                (G.moves s))

    (* evaluate : G.state -> G.est *)
    and evaluate (s : G.state) : G.est =
        case G.status s of
             G.Over(v) => G.Definitely(v)
           | G.In_play => edgeval(search s)

    (* recall:  the signature requires that s be In_play. *)
    val next_move = edgemove o search
end  (* FullMiniMax *)


functor MiniMax (Settings : SETTINGS) : PLAYER =
struct
    structure Game = Settings.Game

    (* We also abbreviate Game as G, to keep the notation simple below. *)
    structure G = Game

    type edge = G.move * G.est
    fun edgemove (m,v) = m
    fun edgeval (m,v) = v

    (* lesseq : G.est * G.est -> bool *)
    fun lesseq(x, y) = (x=y) orelse 
         case (x, y) of
              (G.Definitely(G.Winner G.Minnie), _) => true
            | (_, G.Definitely(G.Winner G.Maxie)) => true
            | (G.Guess n, G.Definitely G.Draw) => (n <= 0)
            | (G.Definitely G.Draw, G.Guess m) => (0 <= m)
            | (G.Guess n, G.Guess m) => (n <= m)
            | (_, _) => false

    (* max : edge * edge -> edge   ,    min : edge * edge -> edge *)
    (* Note:  when values are equal, we stick with first edge.    *)
    fun max (e1, e2) = if lesseq(edgeval e2, edgeval e1) then e1 else e2
    fun min (e1, e2) = if lesseq(edgeval e1, edgeval e2) then e1 else e2

    (* choose : G.player -> edge Seq.seq -> edge *)
    fun choose G.Maxie  = Seq.reduce1 max
      | choose G.Minnie = Seq.reduce1 min

    (* search : int -> G.state -> edge         *)
    (* REQUIRES: d > 0, status(s) == In_play   *)
    (* Technically, d <= 0 is ok, but then search is unbounded. *)
    fun search (d : int) (s : G.state) : edge =
        choose (G.player s)
               (Seq.map
                (fn m => (m, evaluate (d - 1) (G.make_move(s,m))))
                (G.moves s))

    (* evaluate : int -> G.state -> G.est *)
    (* REQUIRES: d >= 0.                  *)
    and evaluate (d : int) (s : G.state) : G.est =
        case (G.status s, d) of
             (G.Over(v), _) => G.Definitely(v)
           | (G.In_play, 0) => G.estimate s
           | (G.In_play, _) => edgeval(search d s)

    (* recall:  the signature requires that s be In_play. *)
    val next_move = edgemove o (search Settings.depth)

end  (* MiniMax *)


functor HumanPlayer (G : GAME) : PLAYER =
struct
  structure Game = G

  fun readmove () = 
        case TextIO.inputLine TextIO.stdIn of
             NONE => raise Fail "early input termination; aborting"
           | SOME(str) => SOME(String.substring(str, 0, String.size(str)-1))
  (* This strips off a trailing newline character, as required by G.parse_move. *)
    
  fun parsemove(state, NONE) = NONE
    | parsemove(state, SOME(str)) = G.parse_move state str

  fun player_to_string (G.Maxie)  = "Maxie"
    | player_to_string (G.Minnie) = "Minnie"
             
  fun next_move state =
      let 
         val _ = print(player_to_string(G.player state) ^ ", please type your move: ")
      in
         case parsemove(state, readmove()) of
              SOME(m) => m
            | NONE => (print "Something is wrong; bad input or bad move.\n";
                       next_move state)
      end
end  (* HumanPlayer *)


signature TWO_PLAYERS =
sig
    structure Maxie  : PLAYER  (* parameter *)
    structure Minnie : PLAYER  (* parameter *)
    sharing type Maxie.Game.state = Minnie.Game.state
    sharing type Maxie.Game.move = Minnie.Game.move

    (* Alternatively, one could replace those last two lines with:
          sharing Maxie.Game = Minnie.Game
    *)
end


signature GO =
sig
   val go : unit -> unit
end


functor Referee (P : TWO_PLAYERS) : GO =
struct
    structure G = P.Maxie.Game
    structure H = P.Minnie.Game

    (* run:  G.state -> string *)
    fun run s =
        case (G.status s, G.player s) of
             (G.Over(v), _) => G.outcome_to_string(v)
           | (G.In_play, G.Maxie) => run(G.make_move(s, P.Maxie.next_move s))
           | (G.In_play, G.Minnie) => run(H.make_move(s, P.Minnie.next_move s))

    fun go () = print(run(G.start) ^ "\n")
end

functor VerboseReferee (P : TWO_PLAYERS) : GO =
struct
   structure G = P.Maxie.Game

   (* play : state -> unit *)
   fun play s = 
       case G.status s of
            G.Over(v) => print(G.outcome_to_string(v) ^ "\n")
          | G.In_play =>
            let
               val (s_to_string, m_to_string, next_move, make_move) =
                   (case G.player s of
                         G.Maxie => 
                             (P.Maxie.Game.state_to_string, P.Maxie.Game.move_to_string, 
                              P.Maxie.next_move, P.Maxie.Game.make_move)
                       | G.Minnie => 
                             (P.Minnie.Game.state_to_string, P.Minnie.Game.move_to_string, 
                              P.Minnie.next_move, P.Minnie.Game.make_move))
               val m = next_move(s)
               val s' = make_move(s, m)
            in
               (print ("The move is " ^ m_to_string m ^ "\n");
                print (s_to_string s' ^ "\n");
                play s')
            end

   fun go () = 
       let
          val start = G.start
          val _ = print((case G.player start of 
                              G.Maxie => P.Maxie.Game.state_to_string 
                            | G.Minnie => P.Minnie.Game.state_to_string) start ^ "\n")
       in
          play start
       end
end


(************************************************************************)

(* Sample Human vs depth-3 MiniMax for Nim: *)

structure NimH = HumanPlayer(Nim)

structure Set3 : SETTINGS = 
struct 
   structure Game = Nim
   val depth = 3
end

structure Nim3 = MiniMax(Set3)

structure HvMM3 : TWO_PLAYERS = 
struct
   structure Maxie = NimH
   structure Minnie = Nim3
end

structure Nim_HvMM3 = VerboseReferee(HvMM3);

(* To play this game, uncomment the next line. *)

(*
Nim_HvMM3.go();
*)

(************************************************************************)

(* Even though we placed the estimator inside the GAME signature, we do
   have at least a little flexibility in changing that, for different
   players of the same game.  For instance, the following functor
   expects a GAME and produces a new nearly identical GAME, merely
   making the estimator trivial.
*)

functor TrivialEst (G : GAME) : GAME =
struct
  open G   (* This is an instance when using open is good. *)

  (* This estimator is very dumb.  It always guesses 0. *)
  fun estimate s = Guess 0

end

structure NimTrivialEst = TrivialEst(Nim)
structure NimHumanTrivialEst = HumanPlayer(NimTrivialEst)

structure HumanTrivialvMM3 : TWO_PLAYERS = 
struct
   structure Maxie = NimHumanTrivialEst
   structure Minnie = Nim3
end

structure Nim_HumanTrivial_MiniMaxDepth3 = VerboseReferee(HumanTrivialvMM3);

(* To play this game, uncomment the next line.
   It should behave the same as the game below that.
*)

(*
Nim_HumanTrivial_MiniMaxDepth3.go();
*)



(* That previous code uses open, which we discourage in this course,
   since it can accidentally shadow functions.  In the previous functor
   we intentionally shadow the function estimate, while focusing the
   reader's attention on the fact that there is exactly this one change.
   That is good.

   Without open we would perhaps write something like this:
*)

functor TrivialEst' (G : GAME) : GAME =
struct
    datatype player = Minnie | Maxie
    datatype outcome = Winner of player | Draw
    datatype status = Over of outcome | In_play

    fun playerG G.Minnie = Minnie
      | playerG G.Maxie = Maxie

    fun outG (G.Winner p) = Winner (playerG p)
      | outG G.Draw = Draw

    fun statG (G.Over out) = Over (outG out)
      | statG G.In_play = In_play

    type state = G.state
    type move = G.move

    val start = G.start

    val make_move = G.make_move
    val moves = G.moves
    val status = statG o G.status
    val player = playerG o G.player

    datatype est = Definitely of outcome | Guess of int

    (* trivial estimator --- our only operationally meaningful change *)
    fun estimate s = Guess 0

    fun Gplayer Minnie = G.Minnie
      | Gplayer Maxie = G.Maxie

    fun Gout (Winner p) = G.Winner (Gplayer p)
      | Gout Draw = G.Draw

    fun Gstat (Over out) = G.Over (Gout out)
      | Gstat In_play = G.In_play

    fun Gest (Definitely out) = G.Definitely (Gout out)
      | Gest (Guess n) = G.Guess n

    val outcome_to_string = G.outcome_to_string o Gout
    val player_to_string = G.player_to_string o Gplayer
    val move_to_string = G.move_to_string
    val state_to_string = G.state_to_string
    val est_to_string = G.est_to_string o Gest
    val parse_move = G.parse_move
end


structure NimTrivialEst' = TrivialEst'(Nim)
structure NimHumanTrivialEst' = HumanPlayer(NimTrivialEst')

structure HumanTrivialvMM3' : TWO_PLAYERS = 
struct
   structure Maxie = NimHumanTrivialEst'
   structure Minnie = Nim3
end

structure Nim_HumanTrivial_MiniMaxDepth3' = VerboseReferee(HumanTrivialvMM3);


Nim_HumanTrivial_MiniMaxDepth3'.go();


(************************************************************************)
