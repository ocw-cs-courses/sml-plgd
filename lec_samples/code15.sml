(* 15-150, Spring 2020                                                *)
(* Michael Erdmann & Frank Pfenning                                   *)
(* Code for Lecture 15:  Regular Expression Matching with Combinators *)

(************************************************************************)

(* Regular Expressions revisited with staged computation.             *)
(* See Lecture 14 for the unstaged version.                           *)
(* See Lecture 11 for a review of staging.                            *)

(*
  Computations in first stage:
        Decompose a regular expression into its constituent parts
        (think of the regular expression as a tree),
        build simple matchers for the base cases,
        then glue these together using combinators to create a matcher
        for the overall regular expression.

  Computations in second stage:
        Matching input, accepting/rejecting.
        The second stage decides whether a given string is in the
        language associated with the regular expression for which
        the first stage has built a matcher
        (conditional on what the initial continuation says).
*)
(************************************************************************)

datatype regexp =
    Char of char
  | One
  | Zero
  | Times of regexp * regexp
  | Plus of regexp * regexp
  | Star of regexp

type matcher = char list -> (char list -> bool) -> bool

val REJECT : matcher = fn _ => fn _ => false
val ACCEPT : matcher = fn cs => fn k => k cs


fun CHECK_FOR (a : char) : matcher =
      fn cs => fn k => case cs of
                         [] => false
                       | (c::cs') => (a=c) andalso (k cs')

(* Here is a version of CHECK_FOR that stages the character check: *)
fun CHECK_FOR (a : char) : matcher =
      fn [] => REJECT []
       | c::cs => if a=c then ACCEPT cs else REJECT (c::cs)


infixr 8 ORELSE
infixr 9 THEN

fun (m1 : matcher) ORELSE (m2 : matcher) : matcher = 
       fn cs => fn k => m1 cs k orelse m2 cs k

fun (m1 : matcher) THEN (m2 : matcher) : matcher = 
       fn cs => fn k => m1 cs (fn cs' => m2 cs' k)


(* Assuming that all regular expressions are in standard form, we  *)
(* might implement Star by using the following REPEAT combinator:  *)
fun REPEAT (m : matcher) : matcher = fn cs => fn k =>
    let
       fun mstar cs' = k cs' orelse m cs' mstar
    in
       mstar cs
    end

(* If we allow arbitrary regular expressions,  *)
(* then we might implement REPEAT like this:   *)
fun REPEAT (m : matcher) : matcher = fn cs => fn k =>
    let
       fun mstar cs' = k cs' orelse
                       m cs' (fn cs'' => not (cs' = cs'') andalso mstar cs'')
    in
       mstar cs
    end


(* Build a matcher from a regexp.
   match : regexp -> char list -> (char list -> bool) -> bool
   Specs are as in Lecture 14.

   Observe how match can traverse the regular expression passed to it, 
   creating a matcher for that regular expression, prior to seeing 
   any character input or continuations.
*)
fun match (Char a) = CHECK_FOR a
  | match One = ACCEPT
  | match Zero = REJECT
  | match (Times (r1, r2)) = (match r1) THEN (match r2)
  | match (Plus (r1, r2)) = (match r1) ORELSE (match r2)
  | match (Star r) = REPEAT (match r)


(* accept : regexp -> string -> bool
   Specs are as in Lecture 14.
   We write code that is a little more complicated than we did in Lecture 14
   in order to take good advantage of the staging now present in match.
*)
   
fun accept (r : regexp) : string -> bool = 
    let
       val m = match r
    in
       fn s => m (String.explode s) List.null
    end


(* Test cases: *)

val Ca = Char(#"a")
val Cb = Char(#"b")

val a1 = accept (Times(Ca,Ca)) (* aa *)
val a2 = accept (Star(Plus(Ca,Cb))) (* (a+b)* *)
val a3 = accept (Times(Times(Times(Star(Plus(Ca,Cb)),Ca),Ca),
                       Star(Plus(Ca,Cb))))
         (* (a+b)*aa(a+b)* *)
val a4 = accept (Times(Plus(Ca,One),Star(Plus(Cb,Times(Cb,Ca)))))
         (* (a+1)(b+ba)* *)

val a5 = accept (Times (Plus (Ca, Times (Ca, Cb)), Plus (Ca, Cb)))
         (* (a + ab)(a + b) *)


val true  = a1 "aa"         (* returns true  *)
val false = a1 "ab"         (* returns false *)
val true  = a2 "abababb"    (* returns true  *)
val false = a2 "abacabb"    (* returns false *)

val true  = a3 "ababbbbabaaabbb"  (* returns true  *)
val false = a4 "ababbbbabaaabbb"  (* returns false *)
val false = a3 "ababbbbabababbb"  (* returns false *)
val true  = a4 "ababbbbabababbb"  (* returns true  *)


(************************************************************************)
