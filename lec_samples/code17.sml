(* 15-150, Spring 2020                           *)
(* Michael Erdmann & Frank Pfenning              *)
(* Code for Lecture 17:  Type Classes, Functors. *)

(* This code uses transparent ascription. *)

(************************************************************************)

(* Ordered type class *)

signature ORDERED =
sig
  type t     (* parameter *)
  val compare : t * t -> order
end


structure IntLt : ORDERED =
struct
  type t = int
  val compare = Int.compare
end


structure IntGt : ORDERED =
struct
  type t = int
  fun compare(x,y) = Int.compare(y,x)
end

structure StringLt : ORDERED =
struct
  type t = string
  val compare = String.compare
end



(* Dictionaries, revisited *)

signature DICT =
sig
  structure Key : ORDERED           (* parameter *)
  type 'a entry = Key.t * 'a        (* concrete *)

  type 'a dict                      (* abstract *)

  val empty : 'a dict
  val lookup : 'a dict -> Key.t -> 'a option
  val insert : 'a dict * 'a entry -> 'a dict
end


(*
   This functor expects an ORDERED key and returns a dictionary,
   implemented as a binary search tree.
*)

functor TreeDict(K : ORDERED) : DICT =
struct
  structure Key = K
  type 'a entry = Key.t * 'a

  datatype 'a tree =  Empty | Node of 'a tree * 'a entry * 'a tree

  type 'a dict = 'a tree

  (* 
    Abstraction function:
      The collection of entries in the binary tree constitutes
      the dictionary.

    Representation Invariant:
     The tree is sorted:
       For every node  Node(left, (key1,value1), right),
       every key in left is LESS than key1, and
       every key in right is GREATER than key1.
  *)

  val empty = Empty

  fun lookup tree key =
    let fun lk (Empty) = NONE
	  | lk (Node(left, (key1,value1), right)) =
              (case Key.compare(key,key1)
		 of EQUAL => SOME(value1)
	          | LESS => lk left
	          | GREATER => lk right)
    in
      lk tree
    end

  fun insert (tree, entry as (key,_)) =
    let fun ins (Empty) = Node(Empty, entry, Empty)
	  | ins (Node(left, entry1 as (key1,_), right)) =
              (case Key.compare(key,key1)
		 of EQUAL => Node(left, entry, right)
	          | LESS => Node(ins left, entry1, right)
                  | GREATER => Node(left, entry1, ins right))
    in 
      ins tree
    end
end  (* functor TreeDict *)


(* Here are some possible structures that we could define: *)

structure IntLtDict = TreeDict(IntLt)
structure IntGtDict = TreeDict(IntGt)
structure StringLtDict = TreeDict(StringLt)

(* But let's test in a separate testing structure: *)
structure TreeLtTest =
struct
  structure T = TreeDict(IntLt)

  val d = T.insert(T.insert(T.empty, (1, "a")), (2, "b"))

  val SOME("a") = T.lookup d 1
  val NONE = T.lookup d 3
end



(**********************************************************************)

(* CAUTION:  In order for a functor to have more than one argument   *)
(*           structure, one needs to use the keyword "structure"     *)
(*           when defining the functor and when using it, as below.  *)
(*           (See also comment about shorthand below.)               *)

(* Create a 2D lexicographic order from two given orders: *)

functor PairOrder (structure Ox : ORDERED
                   structure Oy : ORDERED) : ORDERED =
struct
  type t = Ox.t * Oy.t
  fun compare ((x1,y1), (x2,y2)) =
      (case Ox.compare (x1,x2)
         of EQUAL => Oy.compare (y1,y2)
          | comp => comp)
end


(* Now create an ORDERED 2D grid indexed by strings and integers
   and then a board of strings indexed by that grid:
*)

structure PairTest =
struct

  structure GridOrder = PairOrder(structure Ox = StringLt
                                  structure Oy = IntLt)

  structure Board = TreeDict(GridOrder)

  val board0 = Board.empty
  val board1 = Board.insert(board0, (("A", 1), "Rook"))
  val board2 = Board.insert(board0, (("A", 1), fn x => x + 1))

  val SOME("Rook") = Board.lookup board1 ("A", 1)
  val NONE = Board.lookup board2 ("A", 2)
end

(* Question:  What are the types of board0, board1, and board2?  Why? *)


(**********************************************)

(* Technically, every functor takes a single structure as an argument.
   When we write multiple structures as above we are really writing
   the signature for an implied structure.
   Thus the previous code is actually shorthand for something like this:
*)

functor PairOrder (P : sig
                         structure Ox : ORDERED
                         structure Oy : ORDERED
                       end)
                  : ORDERED
=
struct
  type t = P.Ox.t * P.Oy.t
  fun compare ((x1,y1), (x2,y2)) =
      (case P.Ox.compare (x1,x2)
         of EQUAL => P.Oy.compare (y1,y2)
          | comp => comp)
end


structure PairTest =
struct

  structure GridOrder = PairOrder(struct
                                    structure Ox = StringLt
                                    structure Oy = IntLt
                                  end)

  structure Board = TreeDict(GridOrder)

  val board0 = Board.empty
  val board1 = Board.insert(board0, (("A", 1), "Rook"))
  val board2 = Board.insert(board0, (("A", 1), fn x => x+ 1))

  val SOME("Rook") = Board.lookup board1 ("A", 1)
  val NONE = Board.lookup board2 ("A", 2)
end


(************************************************************************)

(* Some code excerpted from "MoreFunctors.{tex,pdf}" by Steve Brookes: *)  

(* Arithmetic in different bases *)

signature ARITH = 
 sig
     type integer  (* abstract *)
     val rep : int -> integer
     val display : integer -> string
     val add : integer * integer -> integer
     val mult : integer * integer -> integer
 end

structure Dec : ARITH =
 struct
   type digit = int   (* use only digits 0,1,2,3,4,5,6,7,8,9 *)
   type integer = digit list
     
   fun rep 0 = [ ]  |  rep n = (n mod 10) :: rep (n div 10)

   (* carry : digit * integer -> integer *)
   fun carry (0, ps) = ps
    |  carry (c, [ ]) = [c]
    |  carry (c, p::ps) = ((p+c) mod 10) :: carry ((p+c) div 10, ps)

   fun add ([ ], qs) = qs
    |  add (ps, [ ]) = ps
    |  add (p::ps, q::qs) = 
          ((p+q) mod 10) :: carry ((p+q) div 10, add(ps,qs))

   (* times : digit * integer -> integer *)
   fun times (0, _) = [ ]
    |  times (_, [ ]) = [ ]
    |  times (p, q::qs) = 
         ((p * q) mod 10) :: carry ((p * q) div 10, times (p, qs))

   fun mult ([ ], _) = [ ]
    |  mult (_, [ ]) = [ ]
    |  mult (p::ps, qs) = add (times(p, qs), 0 :: mult (ps, qs))
   
   fun display [ ] = "0" 
    |  display L = foldl (fn (d, s) => Int.toString d ^ s) "" L
 end

(* fact : int -> integer *)
fun fact n =
    if n=0 then Dec.rep 1 else Dec.mult (Dec.rep n, fact (n-1))

(* val _ = Control.Print.stringDepth := 2000 *)

val "720" = Dec.display (fact 6)

structure Binary : ARITH =
 struct
   type digit = int   (* use only 0 and 1 *)
   type integer = digit list
   fun rep 0 = [ ]  |   rep n = (n mod 2) :: rep (n div 2)

   (* carry : digit * integer -> integer *)
   fun carry (0, ps) = ps
    |  carry (c, [ ]) = [c]

    |  carry (c, p::ps) = ((p+c) mod 2) :: carry ((p+c) div 2, ps)

   fun add ([ ], qs) = qs
    |  add (ps, [ ]) = ps
    |  add (p::ps, q::qs) = 
          ((p+q) mod 2) :: carry ((p+q) div 2, add (ps,qs))

   (* times : digit * integer -> integer *)
   fun times (0, _) = [ ]
    |  times (_, [ ]) = [ ]
    |  times (p, q::qs) = 
            ((p * q) mod 2) :: carry ((p * q) div 2, times (p, qs))

   fun mult ([ ], _) = [ ]
    |  mult (_, [ ]) = [ ]
    |  mult (p::ps, qs) = add (times(p, qs), 0 :: mult (ps,qs))

   fun display [ ] = "0" 
     | display L = foldl (fn (d, s) => Int.toString d ^ s) "" L
 end

structure B = Binary

(* bfact : int -> B.integer *)
fun bfact n =
    if n=0 then B.rep 1 else B.mult (B.rep n, bfact (n-1))
    
val "110110011000010010110010011101100001110010101110111000010010000000110100101010010100011010101010010111011110110100100000011010001011011101001011001101110111110011010011100001110101100100001101101011011001010010100001110100011001000011100110111110001000000111001000101110100010101010111000011001100101010010100001000001100011011101100101100111011011100101110110100101110111010001011000000101110101000100111001101011100011000011010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
 = B.display(bfact 100)
val "11000" = B.display(bfact 4)


signature BASE =
 sig
   val base : int   (* parameter *)
 end


functor Digits(B : BASE) : ARITH =
 struct
   val b = B.base     (* REQUIRE:  b > 1 *)
   type digit = int   (* use only digits 0 through b-1 *)
   type integer = digit list

   fun rep 0 = [ ]
    |  rep n = (n mod b) :: rep (n div b)

   (* carry : digit * integer -> integer *)
   fun carry (0, ps) = ps
    |  carry (c, [ ]) = [c]
    |  carry (c, p::ps) = ((p+c) mod b) :: carry((p+c) div b, ps)

   fun add ([ ], qs) = qs
    |  add (ps, [ ]) = ps
    |  add (p::ps, q::qs) = ((p+q) mod b) :: carry((p+q) div b, add(ps,qs))

   (* times : digit * integer -> integer *)
   fun times (0, qs) = [ ]
    |  times (p, [ ]) = [ ]
    |  times (p, q::qs) = ((p * q) mod b) :: carry((p * q) div b, times(p, qs))

   fun mult  ([ ], _) = [ ]
    |  mult (_, [ ]) = [ ]
    |  mult (p::ps, qs) = add (times(p, qs), 0 :: mult (ps,qs))

   fun display [ ] = "0" 
    |  display L =  foldl (fn (d, s) => Int.toString d ^ s) "" L  
  end


structure Dec = Digits(struct val base = 10 end)
structure Bin = Digits(struct val base = 2 end)
structure Unary = Digits(struct val base = 1 end)


fun decfact (0:int) : Dec.integer = Dec.rep 1
  | decfact n = Dec.mult(Dec.rep n, decfact(n-1))

fun binfact (0:int) : Bin.integer = Bin.rep 1
  | binfact n = Bin.mult(Bin.rep n, binfact(n-1))

val "24" = Dec.display(decfact 4)
val "11000" = Bin.display(binfact 4)

val "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000"
    =  Dec.display(decfact 100)

val "110110011000010010110010011101100001110010101110111000010010000000110100101010010100011010101010010111011110110100100000011010001011011101001011001101110111110011010011100001110101100100001101101011011001010010100001110100011001000011100110111110001000000111001000101110100010101010111000011001100101010010100001000001100011011101100101100111011011100101110110100101110111010001011000000101110101000100111001101011100011000011010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    =  Bin.display(binfact 100)


fun ufact(n:int) : Unary.integer =
   if n=0 then Unary.rep 1 else Unary.mult(Unary.rep n, ufact(n-1))

(* Caution:  this will loop forever:    (WHY?)
val _ = Unary.display(ufact 4)
*)



structure Base10 = struct val base = 10 end
structure Base2 = struct val base = 2 end
structure Dec = Digits(Base10)
structure Binary = Digits(Base2)

val [2,4] = Dec.rep 42
val [0,1,0,1,0,1] = Bin.rep 42


(***********************************************************************)

(* Monoids *)

signature MONOID =
sig
   type t     (* parameter *)
   val add : t * t -> t
   val zero : t
end

structure AdditiveIntegers : MONOID =
struct
   type t = int
   val add = (op + )
   val zero = 0
end
     
structure MultiplicativeIntegers : MONOID =
struct
   type t = int
   val add = (op * )
   val zero = 1
end

functor Prod (structure A:MONOID  structure B:MONOID) : MONOID =
struct
   type t = A.t * B.t
   val zero = (A.zero, B.zero)
   fun add((a1, b1), (a2, b2)) = (A.add(a1, a2), B.add(b1, b2))
end


structure S = Prod(structure A=AdditiveIntegers
                   structure B=MultiplicativeIntegers)
   

val (3, 28) = S.add((1,4), (2,7))

(************************************************************************)

