(* 15-150, Spring 2020                                             *)
(* Michael Erdmann & Frank Pfenning                                *)
(* Code for Lecture 22:  Streams : Lazy, Demand-Driven Computation *)

(************************************************************************)

signature STREAM =
sig
  type 'a stream   (* abstract *)
  datatype 'a front = Empty | Cons of 'a * 'a stream

  (* Lazy stream construction and exposure *)
  val delay : (unit -> 'a front) -> 'a stream
  val expose : 'a stream -> 'a front

  (* Eager stream construction *)
  (* We ensure that expose(empty) ==> Empty *)
  val empty : 'a stream
  val cons : 'a * 'a stream -> 'a stream

  exception EmptyStream

  val null : 'a stream -> bool
  val hd : 'a stream -> 'a
  val tl : 'a stream -> 'a stream

  val map : ('a -> 'b) -> 'a stream -> 'b stream
  val filter : ('a -> bool) -> 'a stream -> 'a stream
  val exists : ('a -> bool) -> 'a stream -> bool

  val zip : 'a stream * 'b stream -> ('a * 'b) stream

  val take : 'a stream * int -> 'a list
  val drop : 'a stream * int -> 'a stream

  val tabulate : (int -> 'a) -> 'a stream

  val append : 'a stream * 'a stream -> 'a stream
end


structure Stream : STREAM =
struct
  datatype 'a stream = Stream of unit -> 'a front
  and 'a front = Empty | Cons of 'a * 'a stream

  fun delay (d) = Stream(d)
  fun expose (Stream(d)) = d ()

  val empty = Stream(fn () => Empty)
  fun cons (x, s) = Stream(fn () => Cons(x, s))

  exception EmptyStream

  (* functions null, hd, tl, map, filter, exists, take, drop *)
  (* parallel the functions in the List structure *)
  fun null (s) = case (expose s) of
                      Empty => true
                    | _ => false

  fun hd (s) = case (expose s) of
                    Empty => raise EmptyStream
                  | (Cons(x,_)) => x

  fun tl (s) = case (expose s) of
                    Empty => raise EmptyStream
                  | (Cons(_,s)) => s

(* too eager---loops forever on infinite streams
  fun map f s = map' f (expose s)
  and map' f (Empty) = empty
    | map' f (Cons(x,s)) = cons(f x, map f s)
*)

(* better: *)
  fun map f s = delay (fn () => map' f (expose s))
  and map' f (Empty) = Empty
    | map' f (Cons(x,s)) = Cons(f x, map f s)

(* The mutual recursion in the previous implementation is
   a generally useful template to remember, since streams
   and fronts are mutually recursive.
   It is not always needed, however.
   Here is an implementation of map without mutual recursion: *)

  fun map f s =
       delay (fn () =>
               case (expose s) of
                  Empty => Empty
                | Cons(x,s) => Cons (f x, map f s))

  fun filter p s = delay (fn () => filter' p (expose s))
  and filter' p (Empty) = Empty
    | filter' p (Cons(x,s)) =
        if p(x) then Cons(x, filter p s)
        else filter' p (expose s)

  fun exists p s = exists' p (expose s)
  and exists' p (Empty) = false
    | exists' p (Cons(x,s)) =
        p(x) orelse exists p s

  fun zip (s1, s2) = delay (fn () => zip'(expose s1, expose s2))
  and zip' (_, Empty) = Empty
    | zip' (Empty, _) = Empty
    | zip' (Cons(x, s1), Cons(y, s2)) = Cons((x, y), zip(s1, s2))

  (* take (s,n) converts the first n elements of n to a list *)
  (* raises Subscript if n < 0 or n >= length(s) *)
  fun takePos (s, 0) = nil
    | takePos (s, n) = take' (expose s, n)
  and take' (Empty, _) = raise Subscript
    | take' (Cons(x,s), n) = x::takePos(s, n-1)

  fun take (s,n) = if n < 0 then raise Subscript else takePos (s,n)

  fun dropPos (s, 0) = s
    | dropPos (s, n) = drop' (expose s, n)
  and drop' (Empty, _) = raise Subscript
    | drop' (Cons(x,s), n) = dropPos (s, n-1)

  fun drop (s,n) = if n < 0 then raise Subscript else dropPos (s,n)

  fun tabulate f = delay (fn () => tabulate' f)
  and tabulate' f = Cons(f(0), tabulate (fn i => f(i+1)))

  (* "Append" one stream to another.  Of course, if the first stream
      is infinite, we'll never actually get to the second stream. *)
  fun append (s1,s2) = delay (fn () => append' (expose s1, s2))
  and append' (Empty, s2) = expose s2
    | append' (Cons(x,s1), s2) = Cons(x, append (s1, s2))

end

fun ones' () = Stream.Cons(1, Stream.delay ones')
val ones = Stream.delay ones'

fun natsFrom n = Stream.delay (fn () => natsFrom' n)
and natsFrom' n = Stream.Cons(n, natsFrom (n+1))
val nats = natsFrom 0

(* alternative definitions *)
val oness = Stream.tabulate (fn _ => 1)
val natss = Stream.tabulate (fn n => n)
val poss = Stream.map (fn n => n+1) nats
val evens = Stream.map (fn n => 2*n) nats


(* Side comment:  We are performing an addition in natsFrom'.
   If we truly wanted to make that computation lazy as well,
   we might define nats as follows (called natsLazy here):
*)

fun natsFromLazy n = Stream.delay (fn () => natsFromLazy' n)
and natsFromLazy' n = Stream.Cons (n (), natsFromLazy (fn () => n () + 1))
val natsLazy = natsFromLazy (fn () => 0)
    
(* In this course, we merely require that lazy code not perform
   any stream exposures prematurely.  Doing some precomputation,
   such as n+1, when setting up a stream, is fine.
*)


(* This stream is empty *)
val nothing = Stream.filter (fn n => n < 0) nats

(* If one tries to expose an element, the code loops forever: *)
(*
val _ = Stream.expose nothing
*)


(* Inspired by the Sieve of Erathosthenes: *) 

(* notDivides : int -> int -> bool
   REQUIRES: true
   ENSURES: notDivides p q ==> true iff p does not divide q, i.e., 
                               q is not a multiple of p.
*)
fun notDivides p q = (q mod p <> 0)

(* 
   sieve  : int Stream.stream -> int Stream.stream
   sieve' : int Stream.front  -> int Stream.front
*)
fun sieve s = Stream.delay (fn () => sieve' (Stream.expose s))
and sieve' (Stream.Empty) = Stream.Empty
  | sieve' (Stream.Cons(p, s)) =
      Stream.Cons(p, sieve (Stream.filter (notDivides p) s))

(* All the primes as a stream: *)
val primes = sieve (natsFrom 2)

(* The first 100 primes as a list: *)
val p100 = Stream.take(primes, 100)


(* Some test cases: *)

val [1,1,1] = Stream.take(ones, 3)
val [3,4,5] = Stream.take(natsFrom 3, 3)
val [5,6,7,8] = Stream.take(Stream.map (fn n => n+5) natss, 4)
val [2,3,5,7,11,13,17,19,23,29] = Stream.take(primes, 10)



(* And, just for reference, here is an alternate implementation
   of sieve, without mutual recursion: *)

fun sieve s =
     Stream.delay (fn () =>
       case (Stream.expose s) of
          Stream.Empty => Stream.Empty
        | Stream.Cons(p, s) => 
                 Stream.Cons(p, sieve (Stream.filter (notDivides p) s)))

val primes = sieve (natsFrom 2)
val p100 = Stream.take(primes, 100)
val [2,3,5,7,11,13,17,19,23,29] = Stream.take(primes, 10)

(************************************************************************)
