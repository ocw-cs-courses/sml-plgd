(* :: operator is referred to as "cons" as two *)
(* colons. It is the constructor for fixed type *)
(* of lists. *)

(* sum: int list -> int *)
(* REQUIRES: true *)
(* ENSURES: sum (L) = L[0] + sum (L[0:]) *)
fun sum [] = 0
  | sum(x::L) = x + sum L

(* tail recursion *)
(* sum': int list * int -> int *)
(* ENSURES: sum' (L, a) = sum(L) + a *)
fun sum' ([], a) = a
  | sum' (x::L, a) = sum' (L, x+a)

fun Sum L = sum' (L, 0)

fun count[] = 0
  | count(r::R) = sum r + count R

(* sum and Sum are equivalent => so are count and Count *)
fun Count[] = 0
  | Count (r::R) = Sum r + Count R
